package sbp.branching;

import org.junit.jupiter.api.Test;
import sbp.io.MyIOExample;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class TestMyIOExample {
    /**
     *Проверка метода
     * @see MyIOExample#workWithFile при получении метод должен определить,
     * файл ему пришел или директория.
     */
    @Test
    public void workWithFileTest() throws Exception {
        File file = new File("MyFileTest.txt");


        MyIOExample myIOExample = Mockito.mock(MyIOExample.class);

        Mockito.when(myIOExample.workWithFile(file.getName())).thenReturn(true);

        System.out.println(file.getAbsolutePath());

        Assertions.assertEquals(true, file.isFile());
    }

    /**
     * Проверка метода
     * @see  MyIOExample#copyFile на копирование файлов
     */
    @Test
    public void copyFileTest() throws Exception {
        File origin = new File("MyTest.txt");
        File file = new File("MyFileTest.txt");
        File fileCopy = new File("MyFileTestCopy.txt");

        MyIOExample myIOExample = Mockito.mock(MyIOExample.class);

        Mockito.when(myIOExample.copyFile(file.getName(), fileCopy.getName())).thenReturn(true);

        MyIOExample myIOExampleTest = new MyIOExample(myIOExample);

        myIOExampleTest.copyFile(origin.getName(), file.getName());

        Files.copy(Path.of(origin.getName()), new FileOutputStream(fileCopy.getName()));

        List<String> oList = Files.readAllLines(Path.of(origin.getName()));
        List<String> list = Files.readAllLines(Path.of(file.getName()));
        List<String> list1 = Files.readAllLines(Path.of(fileCopy.getName()));

        Assertions.assertEquals(oList, list);
        Assertions.assertEquals(list1, list);
    }
    /**
     * Проверка метода
     * @see  MyIOExample#copyBufferedFile на копирование файлов
     */
    @Test
    public void copyBufferedFileTest() throws Exception {
        File origin = new File("MyTest.txt");
        File file = new File("MyFileTest.txt");
        File fileCopy = new File("MyFileTestCopy.txt");

        MyIOExample myIOExample = Mockito.mock(MyIOExample.class);

        Mockito.when(myIOExample.copyFile(file.getName(), fileCopy.getName())).thenReturn(true);

        MyIOExample myIOExampleTest = new MyIOExample(myIOExample);

        myIOExampleTest.copyFile(origin.getName(), file.getName());

        Files.copy(Path.of(origin.getName()), new FileOutputStream(fileCopy.getName()));

        List<String> oList = Files.readAllLines(Path.of(origin.getName()));
        List<String> list = Files.readAllLines(Path.of(file.getName()));
        List<String> list1 = Files.readAllLines(Path.of(fileCopy.getName()));

        Assertions.assertEquals(oList, list);
        Assertions.assertEquals(list1, list);
    }
    /**
     * Проверка метода
     * @see  MyIOExample#copyFileWithReaderAndWriter на копирование файлов
     */
    @Test
    public void copyFileWithReaderAndWriterTest() throws Exception {
        File origin = new File("MyTest.txt");
        File file = new File("MyFileTest.txt");
        File fileCopy = new File("MyFileTestCopy.txt");

        MyIOExample myIOExample = Mockito.mock(MyIOExample.class);

        Mockito.when(myIOExample.copyFile(file.getName(), fileCopy.getName())).thenReturn(true);

        MyIOExample myIOExampleTest = new MyIOExample(myIOExample);

        myIOExampleTest.copyFile(origin.getName(), file.getName());

        Files.copy(Path.of(origin.getName()), new FileOutputStream(fileCopy.getName()));

        List<String> oList = Files.readAllLines(Path.of(origin.getName()));
        List<String> list = Files.readAllLines(Path.of(file.getName()));
        List<String> list1 = Files.readAllLines(Path.of(fileCopy.getName()));

        Assertions.assertEquals(oList, list);
        Assertions.assertEquals(list1, list);
    }
}
