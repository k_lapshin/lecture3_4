package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest {


    /**
     * Проверка метотода
     * @see MyBranching#maxInt при значении метода Utils#utilFunc2=false
     *
     */
    @Test
    public void maxIntTestMax()
    {
        int i1 = 4;
        int i2 = 3;

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

           MyBranching myBranching = new MyBranching(utilsForTest);
           int result = myBranching.maxInt(i1, i2);

           Assertions.assertEquals(Math.max(i1, i2), result);

    }

    /**
     * Проверка метотода
     * @see MyBranching#maxInt при значении метода Utils#utilFunc2=true
     *
     */
    @Test
    public void maxIntTestZero()
    {
        int i1 = 4;
        int i2 = 3;

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        int result = myBranching.maxInt(i1, i2);

        Assertions.assertEquals(0, result);


    }

    /**
     *Проверка работы метода
     * @see MyBranching#ifElseExample()
     * @return - true, если Utils#utilFunc2() возвращает true
     */
    @Test
    public void ifElseExampleTestTrue()
    {

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();

        Assertions.assertEquals(result, true);

        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();

    }

    /**
     *Проверка работы метода
     * @see MyBranching#ifElseExample()
     * @return - false, если Utils#utilFunc2() возвращает false
     */

    @Test
    public void ifElseExampleTestFalse()
    {
        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();
        Assertions.assertEquals(result, false);

        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка метода
     * @see MyBranching#switchExample на выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     */
    @Test
    public void switchExampleTest1()
    {
        int i = 1;

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc1(anyString())).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        myBranching.switchExample(i);
       // Assertions.assertEquals(utilsForTest.utilFunc1(anyString()), utilsForTest.utilFunc2());
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
    }

    /**
     * Проверка метода
     *@see MyBranching#switchExample на выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     */
    @Test
    public void switchExampleTest2()
    {
        int i = 2;

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        myBranching.switchExample(i);
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();

    }

    /**
     * Проверка метода
     *@see MyBranching#switchExample на выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
     */
    @Test
    public void switchExampleTest0()
    {
        int i = 0;

        Utils utilsForTest = Mockito.mock(Utils.class);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        myBranching.switchExample(i);

        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc1("abc2");

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching1 = new MyBranching(utilsForTest);

        myBranching1.switchExample(i);

        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc1("abc2");


    }
}
